const webpack = require('webpack')
const path = require('path')
const utils = require('./utils')
const config = require('../config')
const vueLoaderConfig = require('./vue-loader.conf')
const entryHtmlPlugins = require('../config/pages')
const SvgStore = require('webpack-svgstore-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const pkg = require('../package.json')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

const setPublicPath = process.env.NODE_ENV === 'production' ?
  config.build.assetsPublicPath : config.dev.assetsPublicPath

module.exports = {
  entry: {
    app: './src/main.js'
  },
  output: {
    path: config.build.assetsRoot,
    filename: '[name].js',
    publicPath: setPublicPath
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src')
    }
  },
  module: {
    rules: [{
      test: /\.vue$/,
      loader: 'vue-loader',
      options: vueLoaderConfig
    }, {
      test: /\.js$/,
      loader: 'babel-loader',
      include: [resolve('src'), resolve('test')]
    }, {
      test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
      loader: 'url-loader',
      exclude: /sprites/,
      options: {
        limit: 10000,
        // name: utils.assetsPath('img/[name].[hash:7].[ext]')
        name: utils.assetsPath('images/[name].[ext]')
      }
    }, {
      test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000,
        name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
      }
    }]
  },
  plugins: [
    new SvgStore({
      svgoOptions: {
        plugins: [
          { removeTitle: true }
        ]
      },
      prefix: ''
    }),
    new webpack.BannerPlugin({
      banner: `@release: [name] v.${pkg.version} [chunkhash]\n@latest: ${new Date().toString()}\n@author: ollejah skillbase`
    }),
    new ManifestPlugin({
      // fileName: 'assets.json',
      // basePath: setPublicPath,
      writeToFileEmit: true
    }),
  ].concat(entryHtmlPlugins)
}
