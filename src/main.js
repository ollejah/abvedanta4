// Enable webpack HMR for extracted resources
module.hot && module.hot.accept()

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
Vue.config.productionTip = false

/**
 * App Constants
 */
const DELAY = 100
const BASE_URL = 'https://ollejah.ru/web/examples/ab4/'

/**
 * Vendors
 */
import { touchAction } from './vendors/h.touch-action'
touchAction()

/**
 * SVG https://github.com/svgstore/webpack-svgstore-plugin
 */
if (process.env.NODE_ENV !== 'development') window.baseUrl = BASE_URL
import svgstore from 'webpack-svgstore-plugin/src/helpers/svgxhr'
const __svg__ = {
  path: 'sprites/icons/*.svg',
  name: 'static/sprite.icons.[hash].svg'
}
svgstore(__svg__)

/**
 * Dom helpers
 */

window.qs = document.querySelector.bind(document)
window.qsa = document.querySelectorAll.bind(document)
Element.prototype.on = Element.prototype.addEventListener

/**
 * App Init
 */
import './styles/app.scss'

/**
 * Document loaded
 */
const $app = qs('#app')
console.time('DOMContentLoaded')
document.addEventListener('DOMContentLoaded', e => {
  $app.style.display = 'flex';
  console.timeEnd('DOMContentLoaded')
})

// console.time('Pace all collectors')
// Pace.on('done', () => {
//   $app.style.display = 'initial';
//   console.timeEnd('Pace all collectors')
// })

// Set frontpage layout
// const $body = document.querySelector('body')
// $body.classList.add('l-frontpage')

/**
 * Site navigation 
 */
const $topMenu = qs('.js-primary-menu')
const $topMenuList = [...$topMenu.querySelectorAll('li.parent')]
const location = window.location.pathname.split('/').pop()
$topMenuList.forEach(($item) => {
  const locationHref = $item.firstChild.getAttribute('href')
  if (location === locationHref) $item.classList.add('is-active')
})

/**
 * Toggle Site navigation 
 */
const $html = qs('html')
const $modal = qs('.js-navigation')
const $pointer = qs('.js-hamburger')

function toggleModal(e) {
  e.preventDefault()
  $html.classList.toggle('is-disabled')
  $pointer.classList.toggle('is-active')
  $modal.classList.toggle('is-active')
  document.addEventListener('keydown', e => {
    if ($html.classList.contains('is-disabled') && e.keyCode == 27) {
      toggleModal(e)
    }
  })
}
qs('.js-hamburger').on('click', e => toggleModal(e))

/**
 * Fetch cards
 */
// import Cards from '@/components/Cards.vue'
if (location === 'media.html') new Vue({
  el: 'v-cards',
  // Асинхронные компоненты
  // ru.vuejs.org/v2/guide/components.html
  components: {
    vCards: () =>
      import ('@/components/Cards.vue')
  }
  // render: h => h(Cards)
})

/*
 * Animated cards
 */
const $cardsList = [...qsa('.c-card')]
let delay = 0
if ($cardsList.length > 0) $cardsList.forEach(($item) => {
  // добавим класс по новой
  if (!$item.classList.contains('-out')) $item.classList.add('-out');
  // анимируем появление
  setTimeout(() => $item.classList.remove('-out'), delay * DELAY)
  delay++
})

/**
 * Filtered cards
 */
// const $filteredCardsList = [...qsa('.js-filtered-cards article')]
// const filteredCards = filterBy => $filteredCardsList.forEach($item => {
//   const dataSet = $item.dataset.filter
//   $item.classList.add('-out');
//   $item.style.display = (dataSet === filterBy || filterBy === 'all') ? '' : 'none'
//   if (dataSet === filterBy || filterBy === 'all') {
//     setTimeout(() => $item.classList.remove('-out'), delay * DELAY)
//     delay++
//   }
// })

// const $cardFiltersList = [...qsa('.js-cards-filters a')]
// $cardFiltersList.forEach(($item, data) => $item.on('click', e => {
//   e.preventDefault()
//   delay = 0 // reset delay for animation
//   const filterBy = $item.hash.substr(1)
//   filteredCards(filterBy)
//   $cardFiltersList.forEach(item => item.classList.remove('is-active'))
//   $item.classList.add('is-active')
// }))

/**
 * Frontpage Carousel
 */
const $slides = qs('.js-carousel-items')

async function setCarouselItems() {
  $slides.style.visibility = 'hidden';
  const $slidesList = [...$slides.querySelectorAll('.js-carousel-item')]
  $slides.style.width = `${100 * $slidesList.length}%`
  return $slidesList
}

async function createCarousel() {
  const $slidesList = await setCarouselItems()
  const currentSlide = index => $slidesList.filter(item =>
    $slidesList.indexOf(item) === index
  )
  const $dots = qs('.js-carousel-dots')
  const $dotsList = [...$dots.querySelectorAll('span')]
  $dotsList.forEach(($dot, index) => $dot.on('click', e => {
    const $slide = currentSlide(index)[0]
    $slides.style.transform = `translateX(-${$slide.offsetLeft}px)`
    $dotsList.forEach(item => item.classList.remove('is-active'))
    $dot.classList.add('is-active')
  }))

  // console.log('done!')
  $slides.style.visibility = 'visible'
};

!!$slides && createCarousel()

/**
 * Video poster
 */
const $videoList = [...document.querySelectorAll('.js-video')]
$videoList.forEach(($video, index) => $video.on('click', e => {
  const $source = $video.querySelector('video')
  $source.paused ? $source.play() : $source.pause()
  const $poster = $video.querySelector('img')
  $poster.style.visibility = $source.paused ? 'visible' : 'hidden';
}))

/**
 * Strict valid target="blank" attribute
 */
// qs('a[href^="http"]:not([href*="' + location.host + '"])').setAttribute('target', '_blank')

/**
 * Вешаем клик на весь блок, содержащий ссылку
 * @param  {DOM} pointer элемент, инициирущий переход
 * @param  {String} children дочерний элемент, где искать ссылку
 * @use clickArea('.b-discount__item');
 */
// const clickArea = function (pointer, children) {
//   const item = pointer.querySelector(children)
//   item && pointer.on('click', () => {
//     if (item.href) window.location.href = item.href
//     return false
//   })
// }
// const $cards = qsa('.c-card');
// [].forEach.call($cards, item => clickArea(item, 'h2 a'))
