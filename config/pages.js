// see http://vuejs-templates.github.io/webpack for documentation.
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}
const distPath = resolve('dist')
const sourcePath = resolve('src')

// HTML
const pages = {
  index: 'Главная',
  about: 'Страница проекта...',
  media: 'Медиа...',
  projects: 'Проекты Ашрама',
  single: 'Страница поста',
  page: 'Страница проекта...',
  blog: 'Листинг постов',
  cards: 'Карточки постов',
  preachers: 'Наши выпускники',
}

const entryHtmlPlugins = Object.keys(pages).map(page => {
  return new HtmlWebpackPlugin({
    chunks: ['vendor', 'app'],
    title: `Ашрам Бхактиведанты | ${pages[page]}`,
    template: `${sourcePath}/index.tmpl`,
    partial: `${page}.html`,
    filename: `${distPath}/${page}.html`,
    inject: true,
    // https://github.com/kangax/html-minifier#options-quick-reference
    minify: (process.env.NODE_ENV === 'production') && {
      removeComments: true,
      collapseWhitespace: true,
      removeAttributeQuotes: true,
      minifyCSS: true,
      minifyJS: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
    },
    // necessary to consistently work with multiple chunks via CommonsChunkPlugin
    chunksSortMode: 'dependency'
  })
})

module.exports = entryHtmlPlugins
