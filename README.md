# Abvedanta

> Ашрам Бхактиведанты

## Build Setup

``` bash
# install yarn 
curl -o- -L https://yarnpkg.com/install.sh | bash

# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn run dev

# build for production with minification
yarn run build

# build for production and view the bundle analyzer report
yarn run build --report

# serve static project
yarn start
```